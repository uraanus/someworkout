/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ds;

/**
 *
 * @author uranus
 */
public class Singelton {

    private static Singelton singelton;
    
    private Singelton(){};
    
    public static Singelton getSing(){
        if(singelton==null){
            singelton = new Singelton();
        }
        
        return singelton;
    }
    
    
}
