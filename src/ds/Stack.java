/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ds;

/**
 *
 * @author uranus
 */
public class Stack {

    private int size;
    private int top;
    private int a[];
    
    public Stack(int size){
        a=new int [size];
        top=-1;
        this.size=size;
    }
    
    private int pop(){
        if(isEmpty())
            System.err.println("Empty sack");
        return a[--top];
              
    }
    
    public void push(int pushedNumber){
        if(isFull())
            return;
        a[++top]=pushedNumber;
    }
    private boolean isEmpty(){
        return top == -1;
    }
    
    public boolean isFull(){
        return top == size-1;
    }
    
    public void printStack(){
        for(int i=0 ; i<top+1; i++){
            System.out.println(a[i]);
        }
    }
    
    public void peek(){
        if(!isEmpty())
            System.out.println("Peek is :"+ a[top]);
        else 
            System.out.println("Stack is Empty!");
    }
    public static void main(String args[]){
        Stack stack = new Stack(6);
        
        stack.push(1);		// Inserting 1 in the stack
	stack.push(2);
        
        stack.pop();
        stack.push(3);
        stack.peek();
        //System.out.println(stack.isFull());
        stack.printStack();
    }
    
}
