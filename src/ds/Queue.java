/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ds;

/**
 *
 * @author uranus
 */
public class Queue {

    private int a[];
    private int capacity;
    private int front;
    private int last;
    private int size;

    public Queue(int size) {
        a=new int[size];
        front = -1;
        last = -1;
        this.size = size;
    }
    
    public boolean isFull(){
        return last+1 == size;
    }
    public void enqueue(int x){
        if(isFull()){
            System.out.println("Queue is full");
            System.exit(1);
        }
        if(last == -1){
            front =0;
            last =0 ;
            a[front] = x;
        }else if(last +1 < size){
            a[++last] = x;
        }
     
    }

    public boolean isEmpty(){
        return front == -1;
    }
    
    public int dequeue(){
        if(isEmpty()){
            System.out.println("Queue is empty");
            System.exit(1);
        }
        int temp = a[front];
        if(front == last){
            front=-1;
            last=-1;
        }else {
            front ++;
        }
        
        return temp;
        
    }
    
    public int len(){
        return (last - front ) + 1 ;
    }
    
}
